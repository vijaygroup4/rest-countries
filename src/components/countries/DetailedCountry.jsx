import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import GoBackButton from "../../helpers/GoBackButton";
import "../../styles/DetailedCountry.css";

const DetailedCountry = ({ setError, isLoading, setIsLoading, countryCodes }) => {
  const { cca3 } = useParams();
  const [country, setCountry] = useState(null);

  let api_url = `https://restcountries.com/v3.1/alpha/${cca3}`;

  async function fetchCountryDetails() {
    try {
      setIsLoading(true);
      let response = await fetch(api_url);
      if (!response.ok) {
        throw new Error("error occurred");
      }
      let data = await response.json();
      let countryData = data[0];
      let nativeNameObject = countryData.name.nativeName;
      let finalKey = Object.keys(nativeNameObject)[Object.keys(nativeNameObject).length - 1];
      let currenciesObject = countryData.currencies;
      let startingKey = Object.keys(currenciesObject)[0];
      let languagesObject = countryData.languages;
      let languages = Object.keys(languagesObject)
        .map((language) => {
          return languagesObject[language];
        })
        .sort()
        .join(", ");

      let borderCountries = [];
      if (countryData.borders) {
        Object.keys(countryCodes).forEach((cca3) => {
          if (countryData.borders.includes(cca3)) {
            borderCountries.push({
              cca3,
              name: countryCodes[cca3],
            });
          }
        });
      }
      setCountry({
        svg: countryData.flags.svg,
        name: countryData.name.common,
        nativeName: nativeNameObject[finalKey].common,
        population: countryData.population,
        region: countryData.region,
        subRegion: countryData.subregion,
        capital: countryData.capital || countryData.region,
        topLevelDomain: countryData.tld[0],
        currencies: currenciesObject[startingKey].name,
        languages: languages,
        borderCountries: borderCountries,
      });
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      setError("Error Occurred while fetching country details!");
      console.log(error);
    }
  }

  useEffect(() => {
    fetchCountryDetails();
  }, [cca3]);

  return (
    <>
      <GoBackButton />
      {isLoading && <p className="loading-country-details">Loading Country Details...</p>}
      {country && (
        <div className="detailed-country">
          <div className="image-div">
            <img src={country.svg} alt="country flag" />
          </div>

          <div className="content-div">
            <div className="second-one">
              <div className="second-one-one">
                <h2 className="title">{country.name}</h2>
                <ul>
                  <li>
                    <p className="left">Native Name:</p>
                    <p className="right">{country.nativeName}</p>
                  </li>
                  <li>
                    <p className="left">Population:</p>
                    <p className="right">{country.population.toLocaleString()}</p>
                  </li>
                  <li>
                    <p className="left">Region:</p>
                    <p className="right">{country.region}</p>
                  </li>
                  <li>
                    <p className="left">Sub Region:</p>
                    <p className="right">{country.subRegion}</p>
                  </li>
                  <li>
                    <p className="left">Capital:</p>
                    <p className="right">{country.capital}</p>
                  </li>
                </ul>
              </div>

              <ul className="second-one-second">
                <li>
                  <p className="left">Top Level Domain:</p>
                  <p className="right">{country.topLevelDomain}</p>
                </li>
                <li>
                  <p className="left">Currencies:</p>
                  <p className="right">{country.currencies}</p>
                </li>
                <li>
                  <p className="left">Languages:</p>
                  <p className="right">{country.languages}</p>
                </li>
              </ul>
            </div>
            <div className="second-second">
              <h3>Border Countries:</h3>
              <div className="sub-buttons">
                {country.borderCountries.length > 0 ? (
                  country.borderCountries.map((border) => {
                    return (
                      <Link to={`/country/${border.cca3}`} key={border.cca3}>
                        <button>{border.name}</button>
                      </Link>
                    );
                  })
                ) : (
                  <p className="no-borders">No Border Countries</p>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default DetailedCountry;
