//importing required functions and Components
import React from "react";
import Country from "./Country";
import "../../styles/Countries.css";

//Countries component
const Countries = ({ countries }) => {
  return (
    <div>
      <ul className="countries">
        {countries.length > 0 ? (
          countries.map((country) => {
            return <Country country={country} key={country.cca3} />;
          })
        ) : (
          <p className="no-countries">No such countries found</p>
        )}
      </ul>
    </div>
  );
};

export default Countries;
