//importing React and its styles
import React from "react";
import { NavLink } from "react-router-dom";
import "../../styles/Country.css";

//creating a component called Country
const Country = ({ country }) => {
  let svg = country.flags.svg;
  let title = country.name.common;
  let capital = country.capital ? country.capital[0] : country.region;
  let population = country.population.toLocaleString(); //coverting to human readable format
  let region = country.region;
  let path = `/country/${country.cca3}`;

  return (
    <NavLink
      to={{
        pathname: path,
      }}
      className="country"
    >
      <div className="flag">
        <img src={svg} alt={title} />
      </div>
      <div className="country-info">
        <h1>{title}</h1>
        <li>
          Population: <p>{population}</p>
        </li>
        <li>
          Capital: <p>{capital}</p>
        </li>
        <li>
          Region: <p>{region}</p>
        </li>
      </div>
    </NavLink>
  );
};

export default Country;
