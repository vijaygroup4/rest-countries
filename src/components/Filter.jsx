//importing required methods and components
import React, { useContext } from "react";
import Select from "react-select";
import ThemeContext from "../contexts/ThemeContext";
import "../styles/Find.css";

//Filter component
const Filter = ({ options, placeholder, onClicking }) => {
  const { isDarkMode } = useContext(ThemeContext);

  return (
    <Select //dropdown component
      options={options}
      placeholder={placeholder}
      className="select-dropdown"
      onChange={(clickedOption) => {
        onClicking(clickedOption.value);
      }}
      styles={{
        control: (baseStyles) => ({
          ...baseStyles,
          backgroundColor: isDarkMode ? "hsl(209, 23%, 22%)" : "white",
          border: "none", // Remove the border
          boxShadow: "0 0 3px black", // Add box shadow
        }),
        option: (baseStyles) => ({
          ...baseStyles,
          ":hover": {
            backgroundColor: isDarkMode ? "white" : "black", // Change color on hover
            color: isDarkMode ? "black" : "white", // Change color on hover
          },
        }),
        singleValue: (baseStyles) => ({
          ...baseStyles,
          color: isDarkMode ? "white" : "black", // Set text color of selected value
        }),
        menu: (baseStyles) => ({
          ...baseStyles,
          backgroundColor: isDarkMode ? "hsl(209, 23%, 22%)" : "white",
          marginTop: -25, // Reduce the gap between Select and container
          color: isDarkMode ? "white" : "black", // Change the text color
        }),
        placeholder: (baseStyles) => ({
          ...baseStyles,
          color: isDarkMode ? "white" : "black", // Change the placeholder color
        }),
        dropdownIndicator: (baseStyles) => ({
          ...baseStyles,
          color: isDarkMode ? "white" : "hsl(209, 23%, 22%)", // Change the dropdown symbol color
        }),
        indicatorSeparator: (baseStyles) => ({
          ...baseStyles,
          display: "none", // Hide the line before the dropdown symbol
        }),
      }}
    />
  );
};

export default Filter;
