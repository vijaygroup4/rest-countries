//importing required methods and components
import React, { useState } from "react";
import { FiSearch } from "react-icons/fi";

//Input component
const InputComponent = ({ onClicking }) => {
  const [inputValue, setInputValue] = useState("");

  return (
    <div className="search">
      <label htmlFor="search">
        <FiSearch className="search-icon" />
      </label>
      <input
        type="text"
        id="search"
        value={inputValue}
        placeholder="Search for a country..."
        onChange={(e) => {
          setInputValue(e.target.value);
          onClicking(e.target.value);
        }}
      />
    </div>
  );
};

export default InputComponent;
