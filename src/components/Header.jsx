//importing required functions and icons
import React, { useContext } from "react";
import { IoMoon } from "react-icons/io5";
import { IoMoonOutline } from "react-icons/io5";
import ThemeContext from "../contexts/ThemeContext";
import "../styles/Header.css";

//Header component
const Header = () => {
  const { isDarkMode, toggleTheme } = useContext(ThemeContext);

  //function to toggle dark mode
  const changeTheme = () => {
    if (!isDarkMode) {
      document.body.setAttribute("data-theme", "dark");
      toggleTheme(true);
    } else {
      document.body.setAttribute("data-theme", "light");
      toggleTheme(false);
    }
  };

  return (
    <header className="header">
      <h1>Where in the world?</h1>
      <div className="sun-moon" onClick={changeTheme}>
        <IoMoonOutline className={isDarkMode ? "light-moon invisible" : "light-moon visible"} />
        <IoMoon className={isDarkMode ? "dark-moon visible" : "invisible"} />
        <p>Dark Mode</p>
      </div>
    </header>
  );
};

export default Header;
