//importing required methods and ThemeContext
import React, { useState } from "react";
import ThemeContext from "./ThemeContext";

//ThemeContext Provider
const ThemeContextProvider = ({ children }) => {
  const [isDarkMode, setIsDarkMode] = useState(false);

  //function that toggles the theme
  const toggleTheme = () => {
    setIsDarkMode((previousValue) => {
      return !previousValue;
    });
  };

  return (
    <ThemeContext.Provider
      value={{
        isDarkMode,
        toggleTheme,
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContextProvider;
