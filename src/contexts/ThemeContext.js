//importing React
import React from "react";

//creating the context
const ThemeContext = React.createContext();

export default ThemeContext;
