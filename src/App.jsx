//importing required methods and components
import React, { useState, useEffect } from "react";
import Header from "./components/Header";
import InputComponent from "./components/InputComponent.jsx";
import Countries from "./components/countries/Countries";
import { Routes, Route } from "react-router-dom";
import DetailedCountry from "./components/countries/DetailedCountry";
import SortButton from "./helpers/SortButton.jsx";
import "./App.css";
import Filter from "./components/Filter";

//App component
function App() {
  const [originalCountries, setOriginalCountries] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [countryInfo, setCountryInfo] = useState({
    countryName: null,
    regionName: null,
    subRegionName: null,
    byPopulation: true,
    byArea: true,
    isAscending: true,
  });

  let regionSubRegion = {};
  let countryCodes = {};
  originalCountries.forEach((country) => {
    let { region, subregion } = country;
    if (regionSubRegion[region]) {
      if (!regionSubRegion[region].includes(subregion)) {
        regionSubRegion[region].push(subregion);
      }
    } else {
      regionSubRegion[region] = [subregion];
    }

    countryCodes[country.cca3] = country.name.common;
  });

  //options for selecting region
  let options = Object.keys(regionSubRegion).map((key) => {
    return {
      value: key,
      label: key,
    };
  });

  //options for selecting subregion
  let subOptions = countryInfo.regionName
    ? regionSubRegion[countryInfo.regionName].map((key) => {
        return {
          value: key,
          label: key,
        };
      })
    : [];

  const API_URL = "https://restcountries.com/v3.1/all";
  useEffect(() => {
    //fetching the countries data
    async function fetchData() {
      try {
        setIsLoading(true);
        const response = await fetch(API_URL);
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        const data = await response.json();
        setOriginalCountries(data);
        setIsLoading(false);
      } catch (error) {
        setIsLoading(false);
        setError("Error occured!");
        console.log(error);
      }
    }
    fetchData();
  }, []);

  let { countryName, regionName, subRegionName, byPopulation, byArea, isAscending } = countryInfo;
  let countries = originalCountries.filter((country) => {
    return (
      (!countryName || country.name.common.toLowerCase().includes(countryName.toLowerCase())) &&
      (!regionName || country.region === regionName) &&
      (!subRegionName || country.subregion === subRegionName)
    );
  });

  if (byArea) {
    countries.sort((a, b) => (isAscending ? a.area - b.area : b.area - a.area));
  } else if (byPopulation) {
    countries.sort((a, b) => (isAscending ? a.population - b.population : b.population - a.population));
  }

  const inputHandler = (value) => {
    setCountryInfo((prevObject) => {
      return {
        ...prevObject,
        countryName: value,
      };
    });
  };

  const regionHandler = (value) => {
    setCountryInfo((prevObject) => {
      return {
        ...prevObject,
        regionName: value,
      };
    });
  };

  const subregionHandler = (value) => {
    setCountryInfo((prevObject) => {
      return {
        ...prevObject,
        subRegionName: value,
      };
    });
  };

  const sortPopulationHandler = () => {
    setCountryInfo((prevObject) => {
      return {
        ...prevObject,
        byPopulation: true,
        byArea: false,
        isAscending: !prevObject.isAscending,
      };
    });
  };

  const sortAreaHandler = () => {
    setCountryInfo((prevObject) => {
      return {
        ...prevObject,
        byPopulation: false,
        byArea: true,
        isAscending: !prevObject.isAscending,
      };
    });
  };

  return (
    <>
      {error ? (
        <p className="error">{error}</p>
      ) : (
        <>
          <Header></Header>
          <Routes>
            <Route
              path="/"
              exact
              element={
                <>
                  <div className="input-filter">
                    <InputComponent className="input" onClicking={inputHandler}></InputComponent>
                    <Filter options={options} className="region" placeholder={`select by region`} setCountryInfo={setCountryInfo} onClicking={regionHandler}></Filter>
                  </div>
                  {countryInfo.regionName && (
                    <div className="subregion">
                      <Filter className="subregion-filter" placeholder={`select by subregion`} options={subOptions} onClicking={subregionHandler}></Filter>
                      <SortButton className="sub-buttons" property="population" onClicking={sortPopulationHandler} countryInfo={countryInfo}></SortButton>
                      <SortButton className="sub-buttons" property="area" onClicking={sortAreaHandler} countryInfo={countryInfo}></SortButton>
                    </div>
                  )}
                  {isLoading ? <p className="loading">Loading...</p> : <Countries countries={countries}></Countries>}
                </>
              }
            ></Route>
            <Route path="country/:cca3" exact element={<DetailedCountry setError={setError} isLoading={isLoading} setIsLoading={setIsLoading} countryCodes={countryCodes} />}></Route>
            <Route path="*" element={<p className="no-page">No Page Found with this url!</p>} />
          </Routes>
        </>
      )}
    </>
  );
}

export default App;
