//importing React
import React from "react";
import "../styles/Button.css";

//Button component
const SortButton = ({ property, countryInfo, onClicking }) => {
  return (
    <button className="button" onClick={onClicking} id={property}>
      sort by {property} in {countryInfo.isAscending ? `descending` : `ascending`}
    </button>
  );
};

export default SortButton;
