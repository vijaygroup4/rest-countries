//importing required methods and components
import React from "react";
import { useNavigate } from "react-router-dom";
import { FaArrowLeft } from "react-icons/fa";
import "../styles/GoBackButton.css";

//GoBackButton Component
const GoBackButton = () => {
  const navigate = useNavigate();

  //function that can return to previous page
  function goBackHandler() {
    navigate("/"); // Navigate back one step
  }

  return (
    <button className="goback-button" onClick={goBackHandler}>
      <FaArrowLeft className="left-arrow" />
      Back
    </button>
  );
};

export default GoBackButton;
